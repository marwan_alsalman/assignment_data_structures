#ifndef BINARYSEACHTREE_H_INCLUDED
#define BINARYSEACHTREE_H_INCLUDED
#include <iostream>
#include "LinkedList.h"


template<class T>
class BinarySearchTree
{
public:
	BinarySearchTree();
	~BinarySearchTree();

private:
	typedef struct Node
	{
		T data;
		Node* right;
		Node* left;
	};
public:
	void insert(T value);
	T find(T value);
	int size();
	void size_rec(Node * node, int& size);
	LinkedList<T>* traversal_pre_order();
	LinkedList<T>* traversal_in_order();
	LinkedList<T>* traversal_post_order();
	void clear();

private:
	void clear_rec(Node** node);
	void print_pre_order_rec(Node* node, LinkedList<T>& list);
	void print_in_order_rec(Node* node, LinkedList<T>& list);
	void print_post_order_rec(Node* node, LinkedList<T>& list);

	LinkedList<T> *preOrder;
	LinkedList<T> *inOrder;
	LinkedList<T> *postOrder;

private:
	Node* head;
	Node* temp;
};

template <class T>
BinarySearchTree<T>::BinarySearchTree()
	:head(nullptr),
	temp(nullptr)
{
	preOrder = new LinkedList<T>;
	inOrder = new LinkedList<T>;
	postOrder = new LinkedList<T>;
}
template <class T>
BinarySearchTree<T>::~BinarySearchTree()
{
	clear();

	delete preOrder;
	preOrder = nullptr;

	delete inOrder;
	inOrder = nullptr;

	delete postOrder;
	postOrder = nullptr;
}

template <class T>
void BinarySearchTree<T>::insert(T value)
{
	Node* node = new Node;
	node->data = value;
	node->right = nullptr;
	node->left = nullptr;

	if (head)
	{
		temp = head;
		while (temp)
		{
			if (value > temp->data)
			{
				if (temp->right == nullptr)
				{
					temp->right = node;
					break;
				}
				else
					temp = temp->right;
			}
			else
			{
				if (temp->left == nullptr)
				{
					temp->left = node;
					break;
				}
				else
					temp = temp->left;
			}
		}
	}
	else
		head = node;
}

template <class T>
T BinarySearchTree<T>::find(T value)
{
	if (head)
	{
		temp = head;
		while (temp)
		{
			if (value == temp->data)
				return temp->data;

			if (value > temp->data)
				temp = temp->right;
			else
				temp = temp->left;

			if (!temp)
				return 0;
		}
	}
	else
		std::cout << "The tree is empty." << std::endl;
}

template <class T>
int BinarySearchTree<T>::size()
{
	int size = 0;
	size_rec(head, size);

	return size;
}
template <class T>
void BinarySearchTree<T>::size_rec(Node* node, int &size)
{
	if (!node)
		return;
	size_rec(node->left, size);
	size_rec(node->right, size);
	size++;
}

template <class T>
LinkedList<T>* BinarySearchTree<T>::traversal_pre_order()
{

	if (head)
	{
		print_pre_order_rec(head, *preOrder);
		return preOrder;
	}
	std::cout << " " << std::endl;
	std::cout << "The list is empty." << std::endl;
	return preOrder;
}
template <class T>
void BinarySearchTree<T>::print_pre_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;

	list.push_back(node->data);
	print_pre_order_rec(node->left, list);
	print_pre_order_rec(node->right, list);
}

template <class T>
LinkedList<T>* BinarySearchTree<T>::traversal_in_order()
{
	if (head)
	{
		print_in_order_rec(head, *inOrder);
		return inOrder;
	}
	
	std::cout << " " << std::endl;
	std::cout << "The list is empty." << std::endl;
	return inOrder;
}
template <class T>
void BinarySearchTree<T>::print_in_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;

	print_in_order_rec(node->left, list);
	list.push_back(node->data);
	print_in_order_rec(node->right,list);
}



template <class T>
LinkedList<T>* BinarySearchTree<T>::traversal_post_order()
{
	if (head)
	{
		print_post_order_rec(head, *postOrder);
		return postOrder;
	}
	std::cout << " " << std::endl;
	std::cout << "The list is empty." << std::endl;
	return postOrder;
}
template <class T>
void BinarySearchTree<T>::print_post_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;
	print_post_order_rec(node->left, list);
	print_post_order_rec(node->right, list);
	list.push_back(node->data);
}


template <class T>
void BinarySearchTree<T>::clear()
{
	clear_rec(&head);
}


template <class T>
void BinarySearchTree<T>::clear_rec(Node** node)
{
	if ((*node) == nullptr)
		return;

	clear_rec(&(*node)->left);
	clear_rec(&(*node)->right);

	if (!((*node)->left) && !((*node)->right))
	{
		delete ((*node));
		*node = nullptr;
	}
}


#endif 



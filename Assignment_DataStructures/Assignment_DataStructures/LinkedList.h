#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <class T>
class  LinkedList
{
public:
	LinkedList();
	~LinkedList();

private:
	typedef struct Node
	{
		T data;
		Node* next;
	};

public:
	void push_front(T vaule);
	void push_back(T vaule);
	void pop_front();
	void pop_back();
	T find_at(int index);
	T find(T value);
	void clear();
	int size();

	T first();
	T last();
	
private:

	Node* head;
	Node* curr;
	Node* temp;
};



template <class T>
LinkedList<T>::LinkedList()
	:head(nullptr),
	curr(nullptr), 
	temp(nullptr)
{

}

template <class T>
LinkedList<T>::~LinkedList()
{
	clear();
}

template <class T>
T LinkedList<T>::find_at(int index)
{
	if (head)
	{
		int i = 0;
		temp = head;
		while (temp)
		{
			if(i == index)
				return temp->data;
			else
			{
				temp = temp->next;
				i++;

			}
		}

	}
	else
		return 0;

	return 0;
}

template <class T>
void LinkedList<T>::push_front(T value)
{

	Node *node = new Node;
	node->data = value;
	node->next = nullptr;

	if (head != nullptr)
	{
		temp = head;
		head = node;
		head->next = temp;
	}
	else
		head = node;
}

template <class T>
void LinkedList<T>::push_back(T value)
{
	Node *node = new Node;
	node->data = value;
	node->next = nullptr;

	if (head)
	{
		temp = head;
		while (temp->next != nullptr)
		{
			temp = temp->next;
		}
		temp->next = node;
	}
	else
		head = node;

	
}

template <class T>
void LinkedList<T>::pop_front()
{
	
	curr = head->next;
	temp = head;
	head = curr;

	if (head != nullptr)
	{
		curr = nullptr;
		delete curr;
		temp = nullptr;
		delete temp;
		

	}
	
}

template <class T>
void LinkedList<T>::pop_back()
{
	temp = head;
	while (temp)
	{
		if (temp->next->next == nullptr)
		{
			delete temp->next;
			temp->next = nullptr;
			break;
		}
		temp = temp->next;
	}
}

template <class T>
void LinkedList<T>::clear()
{
	if (head)
	{
		temp = head;
		while (temp)
		{
			Node* node = temp->next;
			delete temp;
			temp = nullptr;
			temp = node;
		}
		head = nullptr;
	}
	
}

template <class T>
T LinkedList<T>::find(T value)
{
	if (head)
	{
		temp = head;
		while (temp)
		{
			temp = temp->next;
			if (temp->data == value)
			{
				return value;
			}
		}

	}
	else
		return 0;
}

template <class T>
int LinkedList<T>::size()
{
	int size = 0;
	if (head)
	{
		temp = head;
		while (temp)
		{
			temp = temp->next;
			size++;
		}
		return size;
	}
	else
		return 0;
}

template <class T>
T LinkedList<T>::first()
{
	if (head)
		return head->data;
	else
		return 0;
}

template <class T>
T LinkedList<T>::last()
{
	temp = head;
	while (temp)
	{
		if(temp->next)
			temp = temp->next;
		else if(!temp->next)
			return temp->data;
	}
	if (!head)
		return 0;
}
#endif

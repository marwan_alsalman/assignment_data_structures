#include "stdafx.h"
#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "Unit.h"
#include "vld.h"

template<class T>
void UnitTestingLinkedList(LinkedList<T> list);
template<class T>
void UnitTestingBinarySearchTree(BinarySearchTree<T> list);


int main()
{
	LinkedList<int> list;
	BinarySearchTree<int> bst;

	//LinkedList
	list.push_back(7);
	list.push_front(1);
	list.push_front(2);
	list.push_front(3);
	list.push_front(4);
	list.push_back(8);
	list.push_back(55);
	list.~LinkedList();

	//BinarySearchTree
	bst.insert(58);
	bst.insert(2);
	bst.insert(10);
	bst.insert(19);
	bst.insert(21);
	bst.insert(15);
	bst.insert(40);
	bst.insert(32);
	bst.insert(28);
	bst.insert(33);
	bst.insert(44);
	bst.insert(100);
	bst.insert(66);

	


	UnitTestingBinarySearchTree(bst);
	
	

	char input;
	std::cin >> input;
	return 0;
}


template<class T>
void UnitTestingLinkedList(LinkedList<T> list)
{
	std::cout << " " << std::endl;

	//size()
	verify<int>(7, list.size(), "size()");

	//push_front()
	list.push_front(20);
	verify<int>(20, list.first(), "push_front()");

	//push_back()
	list.push_back(33);
	verify<int>(33, list.last(), "push_back()");

	//pop_back()
	list.pop_back();
	verify<int>(55, list.last(), "pop_back()");

	//pop_front()
	list.pop_front();
	verify<int>(4, list.first(), "pop_font()");

	//clear()
	list.clear();
	verify<int>(0, list.size(), "clear()");
}

template<class T>
void UnitTestingBinarySearchTree(BinarySearchTree<T> list)
{

	LinkedList<int> *preOrder = new LinkedList<int>;
	LinkedList<int> *inOrder = new LinkedList<int>;
	LinkedList<int> *postOrder = new LinkedList<int>;

	for (int i = 0; i < list.size(); i++)
	{
		preOrder->push_back(list.traversal_pre_order()->find_at(i));
	}

	for (int i = 0; i < list.size(); i++)
	{
		inOrder->push_back(list.traversal_in_order()->find_at(i));
	}

	for (int i = 0; i < list.size(); i++)
	{
		postOrder->push_back(list.traversal_post_order()->find_at(i));
	}

	//size()
	verify<int>(13, list.size(), "size()");

	//find()
	verify<int>(33, list.find(33), "find()");

	//traversal_pre_order
	verifyList<int>(preOrder, list.traversal_pre_order(), "traversal_pre_order()");

	//traversal_in_order
	verifyList<int>(inOrder, list.traversal_in_order(), "traversal_in_order()");

	//traversal_post_order
	verifyList<int>(postOrder, list.traversal_post_order(), "traversal_pre_order()");
	//clear()
	list.clear();
	verify<int>(0, list.size(), "clear()");


	delete preOrder;
	preOrder = nullptr;

	delete inOrder;
	inOrder = nullptr;

	delete postOrder;
	postOrder = nullptr;

}
